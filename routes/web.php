<?php

    use App\Http\Controllers\HomeController;
    use App\Http\Controllers\ProductController;
    use App\Http\Controllers\StatusController;
    use App\Http\Controllers\UserController;
    use Illuminate\Support\Facades\Route;
    use App\Http\Controllers\BrandController;
    use App\Http\Controllers\FormController;

    Auth::routes();

    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/user', [HomeController::class, 'index']);

    Route::get('/config', [App\Http\Controllers\ConfigController::class, 'index']);

    Route::get('/reports', [App\Http\Controllers\ReportsController::class, 'index']);


    Route::get('/form', [FormController::class, 'index'])->name('form.index');
    Route::get('/form/autocomplete', [FormController::class, 'autocomplete'])->name('form.autocomplete');
    Route::get('/form/getallproduct', [FormController::class, 'getAllProduct'])->name('form.getallproduct');
    Route::get('/form/autofill', [FormController::class, 'autofill'])->name('form.autofill');

    Route::get('/status', [StatusController::class, 'index'])->name('status.index');
    Route::get('/status/create', [StatusController::class, 'create'])->name('status.store');
    Route::post('/status/store', [StatusController::class, 'store'])->name('status.store');
    Route::get('/status/edit/{id}', [StatusController::class, 'show'])->name('status.show');
    Route::post('/status/edit/{id}', [StatusController::class, 'edit'])->name('status.edit');

    Route::get('/product', [ProductController::class, 'index'])->name('product.index');
    Route::get('/product/create', [ProductController::class, 'create'])->name('product.create');
    Route::post('/product/store', [ProductController::class, 'store'])->name('product.store');
    Route::get('/product/edit/{id}', [ProductController::class, 'show'])->name('product.show');
    Route::post('/product/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');

    Route::get('/brand', [BrandController::class, 'index'])->name('brand.index');
    Route::get('/brand/create', [BrandController::class, 'create'])->name('brand.create');
    Route::post('/brand/store', [BrandController::class, 'store'])->name('brand.store');
    Route::get('/brand/edit/{id}', [BrandController::class, 'show'])->name('brand.show');
    Route::post('/brand/edit/{id}', [BrandController::class, 'edit'])->name('brand.edit');
    Route::post('/brand/destroy/{id}', [BrandController::class, 'destroy'])->name('brand.destroy');

    // import-export:
    Route::get('file-import-export', [UserController::class, 'fileImportExport']);
    Route::post('file-import', [UserController::class, 'fileImport'])->name('file-import');
    Route::get('file-export', [UserController::class, 'fileExport'])->name('file-export');
