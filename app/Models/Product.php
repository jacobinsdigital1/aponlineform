<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Product extends Model
{
    use HasFactory;
    use Eloquence;
    protected $table = 'product';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $searchableColumns = ['sku'];
    protected $fillable = ['id_brand','sku','ax_code','wsp','tradediscount','ap_value','active','hide','created_at','updated_at'];
}
