<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand = Brand::paginate(10);

        return view('brand.index',compact('brand'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brand = Brand::create([
            'name_brand' => $request->name_brand,
            'active' => $request->active,
            'hide' => $request->active ? !$request->active : $request->active
        ]);

        $notification = array(
            'messege'=>'Successfully Insert',
            'alert-type'=>'success'
        );

        return Redirect()->route('brand.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Brand::find($id);

        return view('brand.edit',compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $brand = Brand::find($request->id);

        $brand->active = $request->active;
        $brand->hide = !$request->active;
        $brand->name_brand = $request->name_brand;
        $brand->save();


        $notification = array(
            'messege'=>'Successfully Updated',
            'alert-type'=>'success'
        );

        return Redirect()->route('brand.index')->with($notification);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::find($id);

        $brand->active = 0;
        $brand->hide = 1;
        $brand->save();

        $notification = array(
            'messege'=>'Successfully Delete',
            'alert-type'=>'success'
        );

        return Redirect()->route('brand.index')->with($notification);
    }
}
