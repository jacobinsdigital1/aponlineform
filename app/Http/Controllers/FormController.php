<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type_of_promotion = DB::table('type_of_promotion')->get();
        $region = DB::table('region')->get();
        $brand = DB::table('brand')->get();
        $promotion_objectives = DB::table('promotion_objectives')->get();
        $channel = DB::table('channel')->get();
        $product = DB::table('product')->get();
        $products = array();

        return view('form.index',compact('type_of_promotion','region','brand','promotion_objectives','channel','product','products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function show(Form $form)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function edit(Form $form)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Form $form)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Form  $form
     * @return \Illuminate\Http\Response
     */
    public function destroy(Form $form)
    {
        //
    }

    public function autocomplete(Request $request)
    {
        $output = "";
        $queryValue = $request->get('query');

        if(empty($queryValue))
        {
            return;
        }

        $data = Product::select('*')
            ->where("ax_code","LIKE","%{$queryValue}%")
            ->get();

        if (count($data)>0) {

            $output = '<select class="list-group" id="products_selected" >';

            foreach ($data as $row){

                $output .= '<option class="list-group-item" id="'. $row->id .'">'.$row->sku.'</option>';
            }

            $output .= '</select>';
        }
        else {

            $output .= '<option class="list-group-item">'.'No results'.'</option>';
        }

        return response()->json($output);
    }

    public function getAllProduct()
    {
//        return  Product::select('sku');
          return $product = Product::select('ax_code')->get();

//         return response()->json($product);
    }

    public function autofill(Request $request)
    {
        $product = Product::find($request->id);
        if(!empty($product) && $product->active == 1 && $product->hide == 0)
        {
            // get from product
            $data = new \stdClass();
            $data->item_name = $product->sku;
            $data->ax_code = $product->ax_code;
            $data->wholesales_price_excl_vat = $product->wsp;
            // cal


            $data->discount_amount = $product->wsp - $product->tradediscount > 0 ? $product->wsp - $product->tradediscount : 0;
            $data->net_sale = $data->discount_amount/$product->wsp > 0 ? $data->discount_amount/$product->wsp : 0;
            $data->net_sale = round($data->discount_amount/$product->wsp, 2) > 0 ? round($data->discount_amount/$product->wsp, 2) : 0;
            $data->tradediscount = ((100 - $product->tradediscount)/100) * $product->wsp;
            $data->a_p = $data->discount_amount * $data->tradediscount > 0 ? $data->discount_amount * $data->tradediscount : 0;
            // data add
            $data->id_brand = $product->id_brand;
            $data->sku = $product->sku;
            $data->ap_value = $product->ap_value;


            $products[] = $data;
        }

        return  response()->json($products);
    }
}
