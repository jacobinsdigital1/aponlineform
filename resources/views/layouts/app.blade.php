<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8"/>
    @yield('title')
    <title>A&P ONLINE FORM</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta')
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A&P ONLINE FORM" name="A&P ONLINE FORM"/>
    <meta content="A&P ONLINE FORM" name="MER"/>
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('frontend/assets/images/favicon.ico')}}">
    <!-- Bootstrap Css -->
    <link href="{{ asset('frontend/assets/css/bootstrap.min.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{ asset('frontend/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{ asset('frontend/assets/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css" />

    @yield('css')
</head>
<body data-topbar="dark" data-layout="horizontal">
    <div id="app">

    @yield('content')

    <!-- JAVASCRIPT -->
    <script src="{{ asset('frontend/assets/libs/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/metismenu/metisMenu.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/simplebar/simplebar.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/node-waves/waves.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/app.js')}}"></script>

    @yield('js')

    </div>
</body>
</html>
