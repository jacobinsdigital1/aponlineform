<div class="topnav">
    <div class="container-fluid">
        <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

            <div class="collapse navbar-collapse" id="topnav-menu-content">
                <ul class="navbar-nav">

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-dashboard" role="button">
                            <i class="bx bx-home-circle me-2"></i><span key="t-dashboards">Dashboards</span>
                            <div class="arrow-down"></div>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="topnav-dashboard">
                            <a href="{{ url('/') }}" class="dropdown-item" key="t-default">Dashboards</a>
                            <a href="{{ url('/form') }}" class="dropdown-item" key="t-default">Form</a>
                            <a href="{{ url('/product') }}" class="dropdown-item" key="t-default">Product</a>
                            <a href="{{ url('/reports') }}" class="dropdown-item" key="t-default">Reports</a>
                        </div>
                    </li>

{{--                    <li class="nav-item dropdown">--}}
{{--                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-pages" role="button">--}}
{{--                            <i class="bx bx-customize me-2"></i><span key="t-apps">System</span>--}}
{{--                            <div class="arrow-down"></div>--}}
{{--                        </a>--}}
{{--                        <div class="dropdown-menu" aria-labelledby="topnav-pages">--}}

{{--                            <a href="{{ url('/product') }}" class="dropdown-item" key="t-calendar">Product</a>--}}
{{--                            <a href="{{ url('/status') }}" class="dropdown-item" key="t-calendar">Status</a>--}}
{{--                            <div class="dropdown">--}}
{{--                                <a class="dropdown-item dropdown-toggle arrow-none" href="#" id="topnav-email"--}}
{{--                                   role="button">--}}
{{--                                    <span key="t-email">Select/Option</span> <div class="arrow-down"></div>--}}
{{--                                </a>--}}
{{--                                <div class="dropdown-menu" aria-labelledby="topnav-email">--}}
{{--                                    <a href="#" class="dropdown-item" key="t-inbox">Channel</a>--}}
{{--                                    <a href="{{ route('brand.index') }}" class="dropdown-item" key="t-inbox">Brand</a>--}}
{{--                                    <a href="#" class="dropdown-item" key="t-inbox">Possition</a>--}}
{{--                                    <a href="#" class="dropdown-item" key="t-inbox">Responsible</a>--}}
{{--                                    <a href="#" class="dropdown-item" key="t-inbox">Region</a>--}}
{{--                                    <a href="#" class="dropdown-item" key="t-inbox">Promotion Items</a>--}}
{{--                                    <a href="#" class="dropdown-item" key="t-inbox">Type of Promotion</a>--}}
{{--                                    <a href="#" class="dropdown-item" key="t-inbox">Promotion Objectives</a>--}}

{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <a href="{{ url('/config') }}" class="dropdown-item" key="t-calendar">Configuration</a>--}}


{{--                        </div>--}}
{{--                    </li>--}}

                </ul>
            </div>
        </nav>
    </div>
</div>
