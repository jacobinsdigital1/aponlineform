@extends('layouts.app')

@section('content')
    <div class="account-pages my-5 pt-sm-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card overflow-hidden">
                        <div class="bg-primary bg-soft">
                            <div class="row">
                                <div class="col-7">
                                    <div class="text-primary p-4">
                                        <h5 class="text-primary">{{ __('Reset Password') }}</h5>
                                    </div>
                                </div>
                                <div class="col-5 align-self-end">
                                    <img src="{{ asset('frontend/assets/images/profile-img.png')}}" alt=""
                                         class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div>
                                <a href="#">
                                    <div class="avatar-md profile-user-wid mb-4">
                                            <span class="avatar-title rounded-circle bg-light">
                                                <img src="{{ asset('frontend/assets/images/logo.svg')}}" alt=""
                                                     class="rounded-circle" height="34">
                                            </span>
                                    </div>
                                </a>
                            </div>

                            <div class="p-2">

                                @if (session('status'))
                                    <div class="alert alert-success text-center mb-4" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <form method="POST" class="form-horizontal" action="{{ route('password.email') }}">
                                    @csrf

                                    <div class="mb-3">
                                        <label for="useremail" class="form-label">Email</label>
                                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                                               id="email" placeholder="Enter email" name="email"
                                               value="{{ old('email') }}"
                                               required autocomplete="email" autofocus>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <div class="button-items">
                                        <a href="{{ url('/') }}"
                                           class="btn btn-primary waves-effect waves-light">{{ __('Come Back Home Page') }}</a>
                                        <button type="submit"
                                                class="btn btn-primary waves-effect waves-light">{{ __('Send Password Reset Link') }}</button>

                                    </div>


                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="mt-5 text-center">
                        <p>©
                            <script>document.write(new Date().getFullYear())</script>
                            Design & Develop <i class="mdi mdi-heart text-danger"></i> by Phuc Dev
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection

