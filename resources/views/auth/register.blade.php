@extends('layouts.app')

@section('content')
	<div class="account-pages my-5 pt-sm-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8 col-lg-6 col-xl-5">
					<div class="card overflow-hidden">
						<div class="bg-primary bg-soft">
							<div class="row">
								<div class="col-7">
									<div class="text-primary p-4">
										<h5 class="text-primary"> {{ __('Register') }}</h5>
									</div>
								</div>
								<div class="col-5 align-self-end">
									<img src="{{ asset('frontend/assets/images/profile-img.png')}}" alt=""
										 class="img-fluid">
								</div>
							</div>
						</div>
						<div class="card-body pt-0">
							<div>
								<a href="">
									<div class="avatar-md profile-user-wid mb-4">
											<span class="avatar-title rounded-circle bg-light">
												<img src="{{ asset('frontend/assets/images/logo.svg')}}" alt=""
													 class="rounded-circle"
													 height="34">
											</span>
									</div>
								</a>
							</div>
							<div class="p-2">
								<form method="POST" action="{{ route('register') }}">
									@csrf

									<div class="mb-3">
										<label for="useremail" class="form-label">{{ __('Name') }}</label>
										<input type="text" class="form-control @error('name') is-invalid @enderror "
											   value="{{ old('name') }}" required autocomplete="name" autofocus
											   id="name" placeholder="Enter name" name="name">
										@error('name')
										<div class="invalid-feedback">
											<strong>{{ $message }}</strong>
											</span>
										</div>
										@enderror
									</div>


									<div class="mb-3">
										<label for="useremail"
											   class="form-label">{{ __('E-Mail Address') }}</label></label>
										<input type="email" name="email" value="{{ old('email') }}" required
											   autocomplete="email"
											   class="form-control @error('email') is-invalid @enderror" id="email"
											   placeholder="Enter email">
										@error('email')
										<div class="invalid-feedback">
											<strong>{{ $message }}</strong>
											</span>
										</div>
										@enderror
									</div>


									<div class="mb-3">
										<label for="userpassword" class="form-label">{{ __('Password') }}</label>
										<input type="password"
											   class="form-control @error('password') is-invalid @enderror "
											   id="password" name="password"
											   placeholder="Enter password" required autocomplete="new-password">
										@error('password')
										<div class="invalid-feedback">
											<strong>{{ $message }}</strong>
										</div>
										@enderror
									</div>

									<div class="mb-3">
										<label for="userpassword"
											   class="form-label">{{ __('Confirm Password') }}</label>
										<input type="password" class="form-control" id="password-confirm"
											   name="password_confirmation"
											   required autocomplete="new-password">
										@error('password')
										<div class="invalid-feedback">
											<strong>{{ $message }}</strong>
										</div>
										@enderror
									</div>

									<div class="mt-4 d-grid">
										<button class="btn btn-primary waves-effect waves-light" type="submit">
											Register
										</button>
									</div>


								</form>
							</div>

						</div>
					</div>
					<div class="mt-5 text-center">

						<div>
							<p>Already have an account ? <a href="{{ route('login') }}" class="fw-medium text-primary">
									Login</a></p>
							<p class="mb-0">©
								<script>document.write(new Date().getFullYear())</script>
								AnNamBitly. Design & Develop <i class="mdi mdi-heart text-danger"></i> by Phuc Dev
							</p>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- validation init -->
	<script src="{{ asset('frontend/assets/js/pages/validation.init.js')}}"></script>
@endsection
