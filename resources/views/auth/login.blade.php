@extends('layouts.app')

@section('content')
    <div class="row g-0">

        <div class="col-xl-9">
            <div class="auth-full-bg pt-lg-5 p-4">
                <div class="w-100">
                    <div class="bg-overlay"></div>
                    <div class="d-flex h-100 flex-column">

                        <div class="p-4 mt-auto">
                            <div class="row justify-content-center">
                                <div class="col-lg-7">
                                    <div class="text-center">

                                        <h4 class="mb-3"><i
                                                class="bx bxs-quote-alt-left text-primary h1 align-middle me-3"></i><span
                                                class="text-primary">5k</span>+ Satisfied clients</h4>

                                        <div dir="ltr">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end col -->

        <div class="col-xl-3">
            <div class="auth-full-page-content p-md-5 p-4">
                <div class="w-100">

                    <div class="d-flex flex-column h-100">
                        <div class="mb-4 mb-md-5">
                            <a href="#" class="d-block auth-logo">
                                <img src="{{ asset('frontend/assets/images/logo-dark.png')}}" alt="" height="18"
                                     class="auth-logo-dark">
                                <img src="{{ asset('frontend/assets/images/logo-light.png')}}" alt=""
                                     height="18" class="auth-logo-light">
                            </a>
                        </div>
                        <div class="my-auto">

                            <div>
                                <h5 class="text-primary">Welcome Back !</h5>
                                <p class="text-muted">{{ __('Login') }}</p>
                            </div>

                            <div class="mt-4">
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="username"
                                               class="form-label">{{ __('E-Mail Address') }}</label>
                                        <input type="email"
                                               class="form-control @error('email') is-invalid @enderror "
                                               name="email" id="email" placeholder="Enter email"
                                               value="{{ old('email') }}" required autocomplete="email"
                                               autofocus>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <div class="float-end">
                                            <a href="#" class="text-muted">{{ __('Forgot Your Password?') }}</a>
                                        </div>
                                        <label class="form-label">Password</label>
                                        <div class="input-group auth-pass-inputgroup">
                                            <input type="password" name="password" required
                                                   autocomplete="current-password"
                                                   class="form-control @error('password') is-invalid @enderror"
                                                   placeholder="Enter password" aria-label="Password"
                                                   aria-describedby="password-addon">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
                                            @enderror
                                            <a href="{{ route('password.request') }}" class="btn btn-light "
                                               id="password-addon"><i class="mdi mdi-eye-outline"></i></a>
                                        </div>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox"
                                               id="remember-check" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="form-check-label" for="remember-check">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>

                                    <div class="mt-3 d-grid">
                                        <button class="btn btn-primary waves-effect waves-light"
                                                type="submit">{{ __('Login') }}</button>
                                    </div>

                                </form>
                                <div class="mt-5 text-center">
                                    <p>Don't have an account ? <a href="{{ route('register') }}"
                                                                  class="fw-medium text-primary"> Signup
                                            now </a></p>
                                </div>
                            </div>
                        </div>

                        <div class="mt-4 mt-md-5 text-center">
                            <p class="mb-0">©
                                <script>document.write(new Date().getFullYear())</script>
                                AnNamBitly. Design & Develop <i class="mdi mdi-heart text-danger"></i> by Phuc
                                Dev
                            </p>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
@endsection
