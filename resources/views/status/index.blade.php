@extends('layouts.app')

@section('css')
    <link href="{{ asset('frontend/assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('frontend/assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}"
          rel="stylesheet" type="text/css">
    <link href="{{ asset('frontend/assets/libs/spectrum-colorpicker2/spectrum.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('frontend/assets/libs/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}"
          rel="stylesheet" type="text/css">
    <link href="{{ asset('frontend/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('frontend/assets/libs/@chenfengyuan/datepicker/datepicker.min.css')}}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('frontend/assets/libs/select2/js/select2.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/spectrum-colorpicker2/spectrum.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/@chenfengyuan/datepicker/datepicker.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/pages/form-advanced.init.js')}}"></script>
@endsection

@section('content')

    <!-- Begin page -->
    <div id="layout-wrapper">

        <!-- header start -->
    @include('layouts.header')
    <!--  header end -->

        <!-- leftsidebar start -->
    @include('layouts.leftsidebar')
    <!--  leftsidebar end -->


        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18">Status</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="{{ url('/config') }}">System</a></li>
                                        <li class="breadcrumb-item active">Status</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row mb-2">
                                        <div class="col-sm-4">

                                        </div>
                                        <div class="col-sm-8">
                                            <div class="text-sm-end">
                                                <a href="{{ url('/status/create') }}"
                                                        class="btn btn-success btn-rounded waves-effect waves-light mb-2 me-2">
                                                    <i class="mdi mdi-plus me-1"></i> Add New Status
                                                </a>
                                            </div>
                                        </div><!-- end col-->
                                    </div>

                                    <div class="table-responsive">
                                        <table class="table align-middle table-nowrap table-check">
                                            <thead class="table-light">
                                            <tr>
                                                <th style="width: 20px;" class="align-middle">
                                                    <div class="form-check font-size-16">
                                                        <input class="form-check-input" type="checkbox" id="checkAll">
                                                        <label class="form-check-label" for="checkAll"></label>
                                                    </div>
                                                </th>
                                                <th class="align-middle">#</th>
                                                <th class="align-middle">Name Status</th>
                                                <th class="align-middle">Status</th>
                                                <th class="align-middle">Created_At</th>
                                                <th class="align-middle">Updated_At</th>
                                                <th class="align-middle">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($status as $st)
                                                <tr>
                                                    <td>
                                                        <div class="form-check font-size-16">
                                                            <input class="form-check-input" type="checkbox" id="{{ $st->id }}">
                                                            <label class="form-check-label" for="{{ $st->id }}"></label>
                                                        </div>
                                                    </td>
                                                    <td><a href="" class="text-body fw-bold">{{ $st->id }}</a> </td>
                                                    <td>{{ $st->name_status }}</td>
                                                    <td>{{ $st->active == 1 ? "Active" : "Hide" }}</td>
                                                    <td>
                                                        {{ $st->created_at }}
                                                    </td>
                                                    <td>
                                                        {{ $st->updated_at }}
                                                    </td>
                                                    <td>
                                                        <div class="d-flex gap-3">
                                                            <a href="{{ route('status.show',['id'=> $st->id ]) }}" class="text-success"><i class="mdi mdi-pencil font-size-18"></i></a>
                                                            <a href="" class="text-danger"><i class="mdi mdi-delete font-size-18"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>

                                                @endforeach



                                            </tbody>
                                        </table>
                                    </div>
                                    <ul class="pagination pagination-rounded justify-content-end mb-2">
                                        <li class="page-item disabled">
                                            <a class="page-link" href="" aria-label="Previous">
                                                <i class="mdi mdi-chevron-left"></i>
                                            </a>
                                        </li>
                                        <li class="page-item active"><a class="page-link" href="">1</a></li>
                                        <li class="page-item"><a class="page-link" href="">2</a></li>
                                        <li class="page-item"><a class="page-link" href="">3</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="" aria-label="Next">
                                                <i class="mdi mdi-chevron-right"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->


                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->


        </div>
        <!-- end main content-->

        <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->

    </div>
    <!-- END layout-wrapper -->
@endsection
