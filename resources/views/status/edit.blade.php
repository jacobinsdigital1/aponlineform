@extends('layouts.app')

@section('css')
    <link href="{{ asset('frontend/assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('frontend/assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}"
          rel="stylesheet" type="text/css">
    <link href="{{ asset('frontend/assets/libs/spectrum-colorpicker2/spectrum.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('frontend/assets/libs/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}"
          rel="stylesheet" type="text/css">
    <link href="{{ asset('frontend/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('frontend/assets/libs/@chenfengyuan/datepicker/datepicker.min.css')}}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('frontend/assets/libs/select2/js/select2.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/spectrum-colorpicker2/spectrum.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/@chenfengyuan/datepicker/datepicker.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/pages/form-advanced.init.js')}}"></script>
@endsection

@section('content')

    <!-- Begin page -->
    <div id="layout-wrapper">

        <!-- header start -->
    @include('layouts.header')
    <!--  header end -->

        <!-- leftsidebar start -->
    @include('layouts.leftsidebar')
    <!--  leftsidebar end -->


        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18"> Status Create</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="{{ url('/status') }}">Status</a></li>
                                        <li class="breadcrumb-item active">Create</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    <form action="{{ route('status.edit',['id'=> $status->id ]) }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">


                                        <div class="row mb-4">
                                            <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Name Status</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="name_status" name="name_status" value="{{ $status->name_status }}" class="form-control" placeholder=" Name Status" >
                                            </div>
                                        </div>

                                        <div class="row mb-4">
                                            <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Active</label>
                                            <div class="col-sm-9">
                                                <div class="mt-4">
                                                    <div class="form-check mb-3">
                                                        <input class="form-check-input" type="radio" name="active" value="1" id="formRadios1" {{ $status->active == 1 ? "checked" : "" }}  >
                                                        <label class="form-check-label" for="formRadios1">
                                                            Active
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="active" value="0" id="formRadios2" {{ $status->active != 1 ? "checked" : "" }}  >
                                                        <label class="form-check-label" for="formRadios2">
                                                            InActive
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row justify-content-end text-center">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-primary w-md">Update</button>
                                            </div>
                                        </div>



                                    </div>
                                    <!-- end card body -->
                                </div>
                                <!-- end card -->
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->
                    </form>

                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->


        </div>
        <!-- end main content-->

        <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->

    </div>
    <!-- END layout-wrapper -->
@endsection
