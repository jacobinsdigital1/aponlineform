@extends('layouts.app')

@section('content')
    <!-- Begin page -->
    <div id="layout-wrapper">

        <!-- header start -->
    @include('layouts.header')
    <!--  header end -->

        <!-- leftsidebar start -->
    @include('layouts.leftsidebar')
    <!--  leftsidebar end -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18">Brand</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="{{ url('/') }}">Brand</a></li>
                                        <li class="breadcrumb-item active">Brand List</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->


                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row mb-2">
                                        <div class="col-sm-4">

                                        </div>
                                        <div class="col-sm-8">
                                            <div class="text-sm-end">
                                                <a href="{{ url('/brand/create') }}"
                                                   class="btn btn-success btn-rounded waves-effect waves-light mb-2 me-2">
                                                    <i class="mdi mdi-plus me-1"></i> Add New Status
                                                </a>
                                            </div>
                                        </div><!-- end col-->
                                    </div>

                                    @if(!empty($brand))
                                        <div class="table-responsive">
                                            <table class="table align-middle table-nowrap table-check">
                                                <thead class="table-light">
                                                <tr>
                                                    <th class="align-middle">#</th>
                                                    <th class="align-middle">Name Status</th>
                                                    <th class="align-middle">Status</th>
                                                    <th class="align-middle">Created_At</th>
                                                    <th class="align-middle">Updated_At</th>
                                                    <th class="align-middle">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php
                                                    $i = 0;
                                                @endphp
                                                @foreach($brand as $key => $br)
                                                    <tr>
                                                        <td><a href="" class="text-body fw-bold">{{ $brand->firstItem() + $key  }}</a></td>
                                                        <td>{{ $br->name_brand }}</td>
                                                        <td>{{ $br->active == 1 ? "Active" : "Hide" }}</td>
                                                        <td>
                                                            {{ $br->created_at }}
                                                        </td>
                                                        <td>
                                                            {{ $br->updated_at }}
                                                        </td>
                                                        <td>
                                                            <div class="d-flex gap-3">
                                                                <a href="{{ route('brand.show',['id'=> $br->id ]) }}"
                                                                   class="text-success"><i
                                                                        class="mdi mdi-pencil font-size-18"></i></a>

                                                                <form action="{{ route('brand.destroy',['id'=> $br->id ]) }}" method="POST" id="my_form{{ $br->id}}">
                                                                    @csrf
                                                                    <a href="javascript:{}" onclick=" if(confirm('Are you sure delete?')) { document.getElementById('my_form{{ $br->id}}').submit(); }" class="text-danger">
                                                                        <i class="mdi mdi-delete font-size-18"></i></a>
                                                                </form>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $i++
                                                    @endphp
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        {!! $brand->links() !!}
                                    @else
                                        <h3 class="card-title text-center">No Data </h3>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->


                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->


            <!-- footer start -->
        @include('layouts.footer')
        <!-- footer end -->

        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->
@endsection
