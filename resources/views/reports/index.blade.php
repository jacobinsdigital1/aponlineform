@extends('layouts.app')

@section('content')
    <!-- Begin page -->
    <div id="layout-wrapper">

        <!-- header start -->
    @include('layouts.header')
    <!--  header end -->

        <!-- leftsidebar start -->
    @include('layouts.leftsidebar')
    <!--  leftsidebar end -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18">Reports</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="{{ url('/') }}">A&P-ONLINE-FORM</a></li>
                                        <li class="breadcrumb-item active">Reports</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">


                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->
                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->


            <!-- footer start -->
        @include('layouts.footer')
        <!-- footer end -->

        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->
@endsection
