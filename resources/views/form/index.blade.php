@extends('layouts.app')

@section('css')
    <link href="{{ asset('frontend/assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('frontend/assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}"
          rel="stylesheet" type="text/css">
    <link href="{{ asset('frontend/assets/libs/spectrum-colorpicker2/spectrum.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('frontend/assets/libs/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}"
          rel="stylesheet" type="text/css">
    <link href="{{ asset('frontend/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{ asset('frontend/assets/libs/@chenfengyuan/datepicker/datepicker.min.css')}}" rel="stylesheet">

    {{--    <script src="{{asset('frontend/assets/jqueryui/jquery-ui.js')}}" type="text/javascript"></script>--}}

    {{--    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>--}}
    {{--    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>--}}


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    {{--    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />--}}
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>






@endsection

@section('js')
    <script src="{{ asset('frontend/assets/libs/select2/js/select2.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/spectrum-colorpicker2/spectrum.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/@chenfengyuan/datepicker/datepicker.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/js/pages/form-advanced.init.js')}}"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

@endsection

@section('content')

    <!-- Begin page -->
    <div id="layout-wrapper">

        <!-- header start -->
    @include('layouts.header')
    <!--  header end -->

        <!-- leftsidebar start -->
    @include('layouts.leftsidebar')
    <!--  leftsidebar end -->


        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18">Horizontal Layout</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Layouts</a></li>
                                        <li class="breadcrumb-item active">Horizontal Layout</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row justify-content-center">
                                        <div class="col-xl-12 text-center">
                                            <h4 class="text-primary">ADVERTISING AND PROMOTION ACTIVITY PROPOSAL
                                                FORM</h4>
                                        </div>
                                    </div>
                                    <h4 class="card-title">ANNAM PROFESSIONAL</h4>
                                    <div class="row">
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="mb-4">
                                                        <label>Date of Promotion</label>
                                                        <div class="input-daterange input-group" id="datepicker6"
                                                             data-date-format="dd M, yyyy" data-date-autoclose="true"
                                                             data-provide="datepicker"
                                                             data-date-container='#datepicker6'>
                                                            <input type="text" class="form-control" name="start"
                                                                   placeholder="Start Date"/>
                                                            <input type="text" class="form-control" name="end"
                                                                   placeholder="End Date"/>
                                                        </div>
                                                    </div>
                                                    <div class="mb-4">
                                                        <label class="form-label">Type of Promotion</label>
                                                        <select class="form-control select2">
                                                            @foreach($type_of_promotion as $typeofpromotion)
                                                                <option
                                                                    value="{{ $typeofpromotion->id }}">{{ $typeofpromotion->name_type_of_promotion }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="mb-4">
                                                        <label class="form-label">Region</label>
                                                        <select class="form-control select2">
                                                            @foreach($region as $re)
                                                                <option
                                                                    value="{{ $re->id }}">{{ $re->name_region }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="mb-4">
                                                        <label class="form-label">Brand:</label>
                                                        <p>
                                                            Note: One Brand only per A & P Proposal form
                                                        </p>
                                                        <select class="form-control select2" id="selectbrand" onchange="getselectbrand();">
                                                            @foreach($brand as $br)
                                                                <option
                                                                    value="{{ $br->id }}">{{ $br->name_brand }}</option>
                                                            @endforeach

                                                        </select>
                                                    </div>
                                                    <div class="mb-4">
                                                        <label class="form-label">Promotion Objectives</label>
                                                        <select class="form-control select2">
                                                            @foreach($promotion_objectives as $promotionobjectives)
                                                                <option
                                                                    value="{{ $promotionobjectives->id }}">{{ $promotionobjectives->name_promotion_objectives }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="mb-4">
                                                        <label class="form-label">Channel/ Department</label>
                                                        <select class="form-control select2">
                                                            @foreach($channel as $cha)
                                                                <option
                                                                    value="{{ $cha->id }}">{{ $cha->name_channel }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="mb-4">
                                                        <h4 class="form-label">A&P Number:</h4>
                                                        <p id="id_form">A&P_Emborg0001/21</p>
                                                    </div>
                                                    <div class="mb-4">
                                                        <label class="form-label">Quantitative Target:</label>
                                                        <textarea id="textarea" class="form-control" maxlength="225"
                                                                  rows="3"
                                                                  placeholder="Quantitative Target"></textarea>
                                                    </div>
                                                    <div class="mb-4">
                                                        <label class="form-label">Key Accounts:</label>
                                                        <textarea id="textarea" class="form-control" maxlength="225"
                                                                  rows="3"
                                                                  placeholder="Key Accounts"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <label class="mb-1 ">Why we are doing this promotion (Background)</label>
                                            <textarea id="textarea" class="form-control" maxlength="225" rows="3"
                                                      placeholder="Push Sales Offtake"></textarea>
                                        </div>

                                        <div class="col-md-12 text-center">
                                            <label class="mb-1 ">Mechanics of the Promotion</label>
                                            <textarea id="textarea" class="form-control" maxlength="225" rows="3"
                                                      placeholder="Apply special price 140.000VND/kg for Mozzarella block (F122961) (charge 4% ~ 6.920vnđ to Trade discount, 15.08% ~ 26.080vnđ to A&P)"></textarea>
                                        </div>

                                        <div class="col-xl-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="card-title">Product List</h4>


                                                    <div class="app-search ">
                                                        <div class="position-relative">
                                                            <form>
                                                                <div class="form-group">
                                                                    <label class="mb-1 ">Auto Search
                                                                        Product: </label><br>
                                                                    {{--<input id="tags">--}}
                                                                    <input type="text"
                                                                           style="background-color: rgb(42 48 66);color: #fff;"
                                                                           name="keys" id="keys"
                                                                           placeholder="Enter country name"
                                                                           class="form-control">

                                                                </div>
                                                            </form>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xl-10">
                                                                <div id="country_list"></div>
                                                            </div>
                                                            <div class="col-xl-2">
                                                                <button type="button" name="get_product"
                                                                        id="get_product"
                                                                        class="btn btn-primary waves-effect waves-light">
                                                                    Add
                                                                </button>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <style>
                                                        .list-group-item {
                                                            color: #2a3042;
                                                        }

                                                        select#products_selected {
                                                            width: 100%;
                                                            height: 51px;
                                                            font-size: 15px;
                                                        }
                                                    </style>

                                                    <script type="text/javascript">
                                                        function getselectbrand()
                                                        {
                                                            var string_brand = $('#selectbrand option:selected').text().trim();
                                                            //A&P_Emborg0001/21

                                                            var str = "PB 10 CV 2662";
                                                            string_brand = string_brand.split(" ");
                                                            string_brand = string_brand.join("");
                                                            string_brand = string_brand.toLowerCase();
                                                            var strDate = new Date();
                                                            var shortYear = strDate.getFullYear();
                                                            var twoDigitsYear = parseInt(strDate.getFullYear().toString().substr(2,2), 10);

                                                            var id_form = 'A&P_' + string_brand + '0002'+ twoDigitsYear;
                                                            id_form = '<p id="id_form">' + id_form + '</p>';
                                                            $('#id_form').html(id_form);

                                                        }

                                                        $(document).ready(function () {
                                                            function delay(callback, ms) {
                                                                var timer = 0;

                                                                return function () {
                                                                    var context = this, args = arguments;
                                                                    clearTimeout(timer);
                                                                    timer = setTimeout(function () {
                                                                        callback.apply(context, args);
                                                                    }, ms || 0);
                                                                };
                                                            }

                                                            var ul;
                                                            $('#keys').keyup(delay(function (e) {
                                                                var query = $(this).val();
                                                                $.ajax({
                                                                    url: "{{ route('form.autocomplete') }}",
                                                                    type: "GET",
                                                                    data: {'query': query},
                                                                    success: function (data) {
                                                                        $('#country_list').html(data);
                                                                    }
                                                                })
                                                            }, 1000));

                                                            $(document).on('click', 'li', function () {
                                                                var value = $(this).text();
                                                                $('#country').val(value);
                                                                $('#country_list').html("");
                                                            });



                                                        });

                                                        $(document).ready(function () {

                                                            $(document).ready(function () {
                                                                $("body").on("click", ".delrow", function(){
                                                                    if(confirm('Are You sure delete rows ?  '))
                                                                    {
                                                                        $(this).closest('tr').remove();
                                                                    }
                                                                });




                                                            });

                                                            function addCommas(nStr) {
                                                                nStr += '';
                                                                x = nStr.split('.');
                                                                x1 = x[0];
                                                                x2 = x.length > 1 ? '.' + x[1] : '';
                                                                var rgx = /(\d+)(\d{3})/;
                                                                while (rgx.test(x1)) {
                                                                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                                                                }
                                                                return x1 + x2;
                                                            }

                                                            $('#get_product').click(function (e) {
                                                                var sku = $("#products_selected option:selected").text();
                                                                var id = $("#products_selected option:selected").attr('id');

                                                                $.ajax({
                                                                    url: "{{ route('form.autofill') }}",
                                                                    type: "GET",
                                                                    data: {
                                                                        'id': id,
                                                                        'sku': sku
                                                                    },
                                                                    success: function (data) {


                                                                         let discount = addCommas(  data[0].wholesales_price_excl_vat - data[0].tradediscount );
                                                                         let net_sate = addCommas(  (discount / data[0].wholesales_price_excl_vat)*100 );
                                                                        console.log(discount);

                                                                        var text = '<tr><td><input type="text" class="form-control" id="batch_number" name="batch_number" required></td><td>'  + data[0].item_name + '</td><td>'  + data[0].ax_code + '</td> <td>'  + addCommas( data[0].wholesales_price_excl_vat )  + '</td> <td>'  + addCommas( data[0].tradediscount )  + '</td><td> ' + net_sate +'</td> <td>' + discount + '</td><td>6,920</td> <td>26,080</td><td></td> <td></td> <td><input type="text" class="form-control" value="200" id="quality" name="quality" required></td> <td>34,600,000</td> <td></td><td><input  type="button" value="Delete" class="btn btn-danger delrow"><input  type="button" value="Update" class="btn btn-primary"></td> </tr>';
                                                                        var footer001 = `<tr >
                                                                <td colspan="10"></td>
                                                                <td>Total:</td>
                                                                <td>200</td>
                                                                <td>34,600,000</td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="10"></td>
                                                                <td>23,500</td>
                                                                <td>Amount in USD</td>
                                                                <td>$ 1,472.34</td>
                                                                <td></td>
                                                            </tr>`;
                                                                        var footer002 = `<tr><td colspan="9"></td>
                                                                                            <td>Total:</td>
                                                                                            <td>200</td>
                                                                                            <td>34,600,000</td>
                                                                                            <td></td>
                                                                                            <td></td>
                                                                                        </tr><tr>
                                                                                            <td colspan="9"></td>
                                                                                            <td>23,500</td>
                                                                                            <td>Amount in USD</td>
                                                                                            <td>$ 1,472.34</td>
                                                                                            <td></td>
                                                                                            <td></td>
                                                                                        </tr>`;
                                                                        // var seen = $('#table001 tr');

                                                                        // $('#table001 tr').each(function() {
                                                                        //     var txt = $(this).text();
                                                                        //     if (seen[txt])
                                                                        //     {
                                                                        //         $(this).closest('tr').remove();
                                                                        //
                                                                        //         console.log('delete');
                                                                        //     }
                                                                        //     else
                                                                        //     {
                                                                        //         console.log('delete 222');
                                                                        //         seen[txt] = true;
                                                                        //
                                                                        //     }
                                                                        //
                                                                        // });

                                                                        $('#table001 tbody:first').append(text);
                                                                        $('#listproduct002').append(text);


                                                                        $('#footer001').html(footer001);
                                                                        $('#footer002').html(footer002);




                                                                    }
                                                                })
                                                            });


                                                        });


                                                    </script>


                                                    <div class="table-responsive mt-5">
                                                        <table class="table table-bordered border-primary mb-0"
                                                               id="table001">

                                                            <thead class="table-light">
                                                            <tr>
                                                                <th>Batch number</th>
                                                                <th>Item name</th>
                                                                <th>AX Code</th>
                                                                <th>Wholesales price excl VAT</th>
                                                                <th>Discount price</th>
                                                                <th>Net sale</th>
                                                                <th>Discount Amount</th>
                                                                <th>Trade discount</th>
                                                                <th>A&P</th>
                                                                <th>Average Monthly Sales Volume</th>
                                                                <th>% increase due to Promotion</th>
                                                                <th>Sales volume of Promotion</th>
                                                                <th>Sales value of Promotion - exclude VAT</th>
                                                                <th>Actual Sales value ( liquidation)</th>
                                                                <th>Active</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="listproduct"></tbody>
                                                            <tbody id="footer001"></tbody>


                                                            <tr>
                                                                <th style="border: 1px solid #eff2f7;">Promotion Item
                                                                    Charge Account
                                                                </th>
                                                                <th>Description</th>
                                                                <th>AX Code</th>
                                                                <th>Quantity</th>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                                <th>Cost of the item</th>
                                                                <th>Amount booked to A&P (-VAT)</th>
                                                                <th>VAT ( if have)</th>
                                                                <th>Amount to be claimed from principals</th>
                                                                <th>Actual cost - VAT ( liquidation)</th>
                                                            </tr>


                                                            <div id="table002">
                                                                <tbody id="listproduct002"></tbody>
                                                                <tbody id="footer002"></tbody>
                                                            </div>


                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-12 text-center">
                                            <textarea id="textarea" class="form-control" maxlength="225" rows="3"
                                                      placeholder="Special Instructions: Sales ensure full stock display during rental period"></textarea>
                                        </div>


                                        <div class="col-lg-12 mt-4">
                                            <div class="card border border-primary ">
                                                <div class="card-header bg-transparent border-primary">
                                                    <h5 class="my-0 text-primary">A&P Authority Matrix</h5>
                                                </div>
                                                <div class="card-body">
                                                    <p class="card-text">
                                                        - A&P request 0 to 10 million: approved by Marketing Manager
                                                        <br>
                                                        - A&P request over 10 million to 30 million: approved by Head of
                                                        Departments <br>
                                                        - A&P request above 30 million: approved by Managing
                                                        Director<br>
                                                        In case any position mentioned above is out of office, A&P
                                                        request will be approved by the upper next level.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="text-center m-4">
                                            <button class="btn btn-primary text-center" type="submit">Submit</button>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->


                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->


        </div>
        <!-- end main content-->

        <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->

    </div>
    <!-- END layout-wrapper -->
@endsection
