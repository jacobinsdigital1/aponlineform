@extends('layouts.app')

@section('js')

    <!-- Required datatable js -->
    <script src="{{ asset('frontend/assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('frontend/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/jszip/jszip.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>

    <!-- Responsive examples -->
    <script src="{{ asset('frontend/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{ asset('frontend/assets/js/pages/datatables.init.js')}}"></script>
@endsection

@section('content')
    <!-- Begin page -->
    <div id="layout-wrapper">

        <!-- header start -->
    @include('layouts.header')
    <!--  header end -->

        <!-- leftsidebar start -->
    @include('layouts.leftsidebar')
    <!--  leftsidebar end -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18">Create Product</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="{{ route('product.index') }}">Product</a></li>
                                        <li class="breadcrumb-item active">Create</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <!-- end page title -->
                    <form action="{{ route('product.store') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-body">


                                        <div class="row mb-4">
                                            <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Name Status</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="name_status" name="name_status" class="form-control" placeholder=" Name Status" >
                                            </div>
                                        </div>

                                        <div class="row mb-4">
                                            <label for="horizontal-firstname-input" class="col-sm-3 col-form-label">Active</label>
                                            <div class="col-sm-9">
                                                <div class="mt-4">
                                                    <div class="form-check mb-3">
                                                        <input class="form-check-input" type="radio" name="active" value="1" id="formRadios1" checked>
                                                        <label class="form-check-label" for="formRadios1">
                                                            Active
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="active" value="0" id="formRadios2">
                                                        <label class="form-check-label" for="formRadios2">
                                                            InActive
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row justify-content-end text-center">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-primary w-md">Submit</button>
                                            </div>
                                        </div>



                                    </div>
                                    <!-- end card body -->
                                </div>
                                <!-- end card -->
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->
                    </form>



                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->


            <!-- footer start -->
        @include('layouts.footer')
        <!-- footer end -->

        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->
@endsection
