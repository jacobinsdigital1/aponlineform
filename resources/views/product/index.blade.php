@extends('layouts.app')

@section('js')

    <!-- Required datatable js -->
    <script src="{{ asset('frontend/assets/libs/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('frontend/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/jszip/jszip.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/pdfmake/build/pdfmake.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/pdfmake/build/vfs_fonts.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('frontend/assets/libs/datatables.net-buttons/js/buttons.colVis.min.js')}}"></script>

    <!-- Responsive examples -->
    <script src="{{ asset('frontend/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script
        src="{{ asset('frontend/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{ asset('frontend/assets/js/pages/datatables.init.js')}}"></script>
@endsection

@section('content')
    <!-- Begin page -->
    <div id="layout-wrapper">

        <!-- header start -->
    @include('layouts.header')
    <!--  header end -->

        <!-- leftsidebar start -->
    @include('layouts.leftsidebar')
    <!--  leftsidebar end -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                                <h4 class="mb-sm-0 font-size-18">Product</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="{{ route('product.index') }}">Product</a>
                                        </li>
                                        <li class="breadcrumb-item active">Product</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row mb-2">
                                        <div class="col-sm-4">

                                        </div>
                                        <div class="col-sm-8">
                                            <div class="text-sm-end">
                                                <a href="{{ route('product.create') }}"
                                                   class="btn btn-success btn-rounded waves-effect waves-light mb-2 me-2">
                                                    <i class="mdi mdi-plus me-1"></i> Add New Product
                                                </a>
                                            </div>
                                        </div><!-- end col-->
                                    </div>

                                    @if(!empty($brand))
                                        <div class="table-responsive">
                                            <table class="table align-middle table-nowrap table-check">
                                                <thead class="table-light">
                                                <tr>
                                                    <th class="align-middle">#</th>
                                                    <th class="align-middle">Brand</th>
                                                    <th class="align-middle">SKU</th>
                                                    <th class="align-middle">AX Code</th>
                                                    <th class="align-middle">WSP</th>
                                                    <th class="align-middle">Trade Discount, %</th>
                                                    <th class="align-middle">A&P, Value</th>
                                                    <th class="align-middle">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <tr>
                                                    <td><a href="" class="text-body fw-bold">#SK2540</a></td>
                                                    <td><a href="" class="text-body fw-bold">#SK2540</a></td>
                                                    <td>Neal Matthews</td>
                                                    <td>
                                                        07 Oct, 2019
                                                    </td>
                                                    <td>
                                                        $400
                                                    </td>
                                                    <td>
                                                        34234
                                                    </td>
                                                    <td>
                                                        34234
                                                    </td>
                                                    <td>
                                                        <div class="d-flex gap-3">
                                                            <a href="" class="text-success"><i
                                                                    class="mdi mdi-pencil font-size-18"></i></a>
                                                            <a href="" class="text-danger"><i
                                                                    class="mdi mdi-delete font-size-18"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>


                                                </tbody>
                                            </table>
                                        </div>
                                        <ul class="pagination pagination-rounded justify-content-end mb-2">
                                            <li class="page-item disabled">
                                                <a class="page-link" href="" aria-label="Previous">
                                                    <i class="mdi mdi-chevron-left"></i>
                                                </a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="">1</a></li>
                                            <li class="page-item"><a class="page-link" href="">2</a></li>
                                            <li class="page-item"><a class="page-link" href="">3</a></li>
                                            <li class="page-item">
                                                <a class="page-link" href="" aria-label="Next">
                                                    <i class="mdi mdi-chevron-right"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    @else
                                        <h3 class="card-title text-center">No Data </h3>
                                    @endif


                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- end row -->
                </div> <!-- container-fluid -->
            </div>
            <!-- End Page-content -->


            <!-- footer start -->
        @include('layouts.footer')
        <!-- footer end -->

        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->
@endsection
